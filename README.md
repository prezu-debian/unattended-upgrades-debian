# Role Name

Installs and configures `unattended-upgrades` on Debian GNU/Linux system.

## Role Variables

### custom_matchers

By default, only security updates are enabled. If you want, though, you can
configure custom repository matches. You configure those matches in
`custom_matchers` variable. See the examples below for more details on how to
use this variable.

### email_to (optional)

If you want `unattended-upgrades` to email reports, you may specify the
destination email address in `email_to` variable.

### email_from (optional)

If your MTA locally is configured e.g. to send emails thru an SMTP account,
you might need to specify a `From` email address to match the email you're
using for sending out an email. For example, say you have configured your
system to send emails through `your_private_gmail_account@gmail.com`, you'd set
`email_from` to `your_private_gmail_account@gmail.com`.

### automatic_reboot (optional)

Sometimes after an upgrade a reboot is required for the changes to take effect.
That happens for example when a Linux Kernel got updated. `unattended-upgrades`
allows you to automatically reboot the machine without a prompt when that is
the case.

If you wish for this feature to be turned on, set `automatic_reboot` to `yes`.
The default value of this variable is `no`. So if you do **not** wish to have
it on, either don't set it at all, or explicitly set it to `no`.

## Using in your Playbook

Add the following to your `requirements.yaml` file:

```yaml
- src: git+https://gitlab.com/prezu-debian/unattended-upgrades-debian
  version: v1.1.0
  name: unattended-upgrades-debian
```

And then use `ansible-galaxy` to install it:

```yaml
$ ansible-galaxy install -r requirements.yaml
```

## Example Playbooks

The main way to specify which packages will be auto-upgraded is by means of
their "origin" and "archive". These are taken respectively from the Origin and
Suite fields of the repository's Release file, or can be found in the output
of:

```bash
$ apt-cache policy
Package files:
 100 /var/lib/dpkg/status
     release a=now
 500 http://deb.debian.org/debian bullseye-updates/main amd64 Packages
     release v=11-updates,o=Debian,a=stable-updates,n=bullseye-updates,l=Debian,c=main,b=amd64
     origin deb.debian.org
 500 http://security.debian.org/debian-security bullseye-security/non-free amd64 Packages
     release v=11,o=Debian,a=stable-security,n=bullseye-security,l=Debian-Security,c=non-free,b=amd64
     origin security.debian.org
 500 http://security.debian.org/debian-security bullseye-security/main amd64 Packages
     release v=11,o=Debian,a=stable-security,n=bullseye-security,l=Debian-Security,c=main,b=amd64
     origin security.debian.org
 500 http://deb.debian.org/debian bullseye/contrib amd64 Packages
     release v=11.5,o=Debian,a=stable,n=bullseye,l=Debian,c=contrib,b=amd64
     origin deb.debian.org
 500 http://deb.debian.org/debian bullseye/non-free amd64 Packages
     release v=11.5,o=Debian,a=stable,n=bullseye,l=Debian,c=non-free,b=amd64
     origin deb.debian.org
 500 http://deb.debian.org/debian bullseye/main amd64 Packages
     release v=11.5,o=Debian,a=stable,n=bullseye,l=Debian,c=main,b=amd64
     origin deb.debian.org
Pinned packages:
```

Please, see
[templates/52unattended-upgrades-local](templates/52unattended-upgrades-local)
for more details on how the above can be translated into individual matcher
lines.

So, let's say, you're using self-hosted GitLab using GitLab's Omnibus packages
(instead of Debian's official packages). In such a case, you'd see:

```bash
$ apt-cache policy
...
 500 https://packages.gitlab.com/gitlab/gitlab-ee/debian bullseye/main amd64 Packages
     release v=1,o=packages.gitlab.com/gitlab/gitlab-ee,a=bullseye,n=bullseye,l=gitlab-ee,c=main,b=amd64
     origin packages.gitlab.com
...
```

Let's say, you also want to enable
[Debian Backports](https://backports.debian.org) for Bullseye:

```yaml
...
  roles:
    - role: unattended-upgrades-debian
      vars:
        email_to: example@gmail.com
        email_from: your_system_dedicated_email@gmail.com
        # When automatic_reboot is set to no, or not present at, you'll need
        # to make sure that you reboot the machine if the upgrade requires it
        # (e.g. a Kernel's been upgraded).
        automatic_reboot: yes
        custom_matchers:
          # Debian Backports.
          - '"o=Debian Backports,a=${distro_codename}-backports,l=Debian Backports";'

          # GitLab.
          - '"origin=packages.gitlab.com,suite=stable";'
```

# License

GPLv3
